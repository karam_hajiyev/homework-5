package ibatech.model;

public enum DayOfWeek {
    SUNDAY("SUNDAY") {
        public DayOfWeek next() {
            return MONDAY;
        }
    }, MONDAY("MONDAY") {
        public DayOfWeek next() {
            return TUESDAY;
        }
    }, TUESDAY("TUESDAY") {
        public DayOfWeek next() {
            return WEDNESDAY;
        }
    }, WEDNESDAY("WEDNESDAY") {
        public DayOfWeek next() {
            return THURSDAY;
        }
    }, THURSDAY("THURSDAY") {
        public DayOfWeek next() {
            return FRIDAY;
        }
    }, FRIDAY("FRIDAY") {
        public DayOfWeek next() {
            return SATURDAY;
        }
    }, SATURDAY("SATURDAY") {
        public DayOfWeek next() {
            return SUNDAY;
        }
    };

    private String name;
    private String doIt;

    DayOfWeek(String name) {
        this.name = name;
    }

    DayOfWeek() {
    }

    public String getDoIt() {
        return doIt;
    }

    public void setDoIt(String doIt) {
        this.doIt = doIt;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract DayOfWeek next();

    public String toString() {
        return name + " " + doIt;
    }
}

package ibatech.model;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    static {
        System.out.println("Family class is loading");
    }

    {
        System.out.println("Object of Family class is loading");
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return getMother().equals(family.getMother()) && getFather().equals(family.getFather()) && Arrays.equals(getChildren(), family.getChildren()) && getPet().equals(family.getPet());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    public void addChild(Human child) {
        Human[] anotherArray = new Human[children.length + 1];

        int i, k;
        for (i = 0, k = 0; i < children.length; i++) {
            anotherArray[k] = children[i];
            k++;
        }
        children = anotherArray;
        children[k] = child;
    }

    public boolean deleteChild(Human child) {
        if (children == null || child == null) {
            return false;
        }
        Human[] anotherArray = new Human[children.length - 1];

        for (int i = 0, k = 0; i < children.length; i++) {

            if (children[i] == child) {
                continue;
            }
            anotherArray[k++] = children[i];
        }
        children = anotherArray;
        return true;
    }

    public int countFamily() {
        return children.length + 2;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

}
